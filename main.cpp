#include <fstream>
#include <cstdlib>
#include <cassert>
#include <tuple>
#include <vector>
#include "parseresult.hpp"

struct DefBlockHeader
{
    union
    {
        char Name[4];
        uint32_t Num;
    }        Signature;
    uint32_t Length;
    uint8_t  SpecCompliance;
    uint8_t  Checksum;
    char     OEMID[6];
    char     OEMTableId[8];
    uint32_t OEMRevision;
    uint32_t CreatorId;
    uint32_t CreatorRevision;
} __attribute__((packed));

class String
{
public:
    constexpr String(const char* str, uint32_t size)
        : Str_(str), Size_(size) {}

    // DEBUG
    void Print() const
    {
        printf("%.*s", Size_, Str_);
    }

    char operator[](uint32_t index) const
    {
        assert(index < Size_);
        return Str_[index];
    }

    String operator+(uint32_t x) const
    {
        return String(Str_ + x, Size_ - x);
    }

    const char* GetRaw() const
    {
        return Str_;
    }

    uint32_t GetSize() const
    {
        return Size_;
    }

private:
    const char* Str_;
    const uint32_t Size_;
};

class Buffer
{
public:
    // TODO: temp (no interpreter class yet)
    Buffer(){}

    constexpr Buffer(const uint8_t* ptr, uint32_t size)
        : Ptr_(ptr), Size_(size) {}

    void Consume(uint32_t o)
    {
        if(o < Size_)
        {
            Ptr_ += o;
            Size_ -= o;
        }
        else
        {
            Ptr_ += Size_;
            Size_ = 0;
        }
    }

    bool ConsumeIf(uint8_t data)
    {
        if(Ptr_[0] != data)
            return false;

        Consume(1);

        return true;
    }

    uint8_t operator[](uint32_t index) const
    {
        return Ptr_[index];
    }

    const uint8_t* GetRaw() const
    {
        return Ptr_;
    }

    const char* GetRawCharPtr() const
    {
        return reinterpret_cast<const char*>(Ptr_);
    }

    uint32_t GetSize() const
    {
        return Size_;
    }

private:
    const uint8_t* Ptr_;
    uint32_t Size_;
};

struct NSNode;
extern NSNode RootNode_;

// TODO: locking?
struct NSNode // TODO: how to do values in here? a node can contain a value but does not have to
{
    union
    {
        char Name[4];
        uint32_t Id;
    };
    NSNode* FirstChild;
    NSNode* Next;
    NSNode* Parent;

    constexpr NSNode(const char name[4], NSNode* next, NSNode* parent)
        : Id(0), FirstChild(nullptr), Next(next), Parent(parent)
    {
        Name[0] = name[0];
        Name[1] = name[1];
        Name[2] = name[2];
        Name[3] = name[3];
    }

    constexpr NSNode(const char name[4])
        : NSNode(name, nullptr, nullptr) {}

    NSNode* AddChild(const char name[4])
    {
        NSNode* n = new NSNode(name, Next, this);
        if(!n)
            return nullptr;

        FirstChild = n;

        return n;
    }

    NSNode* Find(const String& path) const
    {
        // See ACPI spec v6.2 section 5.3 "ACPI Namespace"
        // Search rules only apply on single nameseg paths (=> length == 4)
        if(path.GetSize() == 4)
        {
            printf("search rules apply\n");
            assert(false);
        }
        else
        {
            return _Find(path);
        }
    }

    NSNode* _Find(const String& path) const
    {
        // Begin at root
        if(path[0] == '\\')
        {
            printf("begin at root\n");
            return RootNode_._Find(path + 1);
        }
        // Begin at parent
        else if(path[0] == '^')
        {
            printf("begin at parent\n");
            if(!Parent)
                return nullptr;

            return Parent->_Find(path + 1);
        }
        // Multi segment path
        else
        {
            // TODO: this is little endian
            uint32_t id = path[0] | path[1] << 8 | path[2] << 16 | path[3] << 24;
            NSNode* current = FirstChild;
            while(current)
            {
                if(current->Id == id)
                    return current;

                current = current->Next;
            }

            return nullptr;
        }
    }
};

NSNode* AddToNS(const String& path)
{

}

const char ROOT_NAME[4] = "\\";
NSNode RootNode_(ROOT_NAME); // TODO: temp because no interpreter class yet

// TODO: clarify that this is the executioncontext?
struct Context
{
    NSNode& CurrentNSNode;

    constexpr Context()
        : CurrentNSNode(RootNode_) {}
};

// TODO: temp because no interpreter class yet
Buffer buffer;

#define ALWAYS_INLINE __attribute__((always_inline)) inline
#define MATCH_OP(op) if(!buffer.ConsumeIf(op)) return ParseError(AMLError::NO_MATCH);
#define FAIL_IF_ERROR(v) if(v.IsError()) return ParseError(v.GetError());

template<typename R>
static ALWAYS_INLINE ParseResult<R> ParseTemplate(Context& ctx, ParseResult<R> (*t)(Context&), ParseResult<R> (*u)(Context&))
{
    auto res = t(ctx);
    if(res.IsOk() || (res.IsError() && res.GetError() != AMLError::NO_MATCH))
        return res;

    return u(ctx);
}

template<typename R, typename... V>
static ALWAYS_INLINE ParseResult<R> ParseTemplate(Context& ctx, ParseResult<R> (*t)(Context&), ParseResult<R> (*u)(Context&), V... v)
{
    auto res = t(ctx);
    if(res.IsOk() || (res.IsError() && res.GetError() != AMLError::NO_MATCH))
        return res;

    return ParseTemplate(ctx, u, v...);
}

template<typename R>
static ALWAYS_INLINE ParseResult<R> ParseTemplate(ParseResult<R> (*t)(void), ParseResult<R> (*u)(void))
{
    auto res = t();
    if(res.IsOk() || (res.IsError() && res.GetError() != AMLError::NO_MATCH))
        return res;

    return u();
}

template<typename R, typename... V>
static ALWAYS_INLINE ParseResult<R> ParseTemplate(ParseResult<R> (*t)(void), ParseResult<R> (*u)(void), V... v)
{
    auto res = t();
    if(res.IsOk() || (res.IsError() && res.GetError() != AMLError::NO_MATCH))
        return res;

    return ParseTemplate(u, v...);
}

// TODO: errorhandling & overflow/"out of bounds" handling

// TODO: un-tuple
std::tuple<uint32_t, uint32_t> ParsePkgLength(const Buffer& buffer)
{
    uint8_t leadByte = buffer[0];
    uint8_t count = (leadByte >> 6) & 0b11;

    uint32_t size;
    if(count == 0)
        size = leadByte & 0b111111;
    else
    {
        size = (leadByte & 0b1111) | (buffer[1] << 4);
        if(count == 2) size |= (buffer[2] << 12);
        if(count == 3) size |= (buffer[3] << 20);
    }

    return std::tuple<uint32_t, uint32_t>(count + 1, size);
}

bool IsLeadNameChar(char c)
{
    return (c >= 'A' && c <= 'Z') || c == '_';
}

bool IsNameChar(char c)
{
    return (c >= '0' && c <= '9') || IsLeadNameChar(c);
}

bool IsNameSeg()
{
    return IsLeadNameChar(buffer[0]) && IsNameChar(buffer[1]) && IsNameChar(buffer[2]) && IsNameChar(buffer[3]);
}

ParseResult<String> ParseNameSeg()
{
    if(IsNameSeg())
    {
        buffer.Consume(4);
        return ParseOk(String(buffer.GetRawCharPtr() - 4, 4));
    }

    return ParseError(AMLError::NO_MATCH);
}

ParseResult<String> ParseDualNamePath()
{
    MATCH_OP(0x2E);

    auto ns1 = ParseNameSeg();
    if(ns1.IsError())
        return ParseError(AMLError::INVALID_NAME);

    auto ns2 = ParseNameSeg();
    if(ns2.IsError())
        return ParseError(AMLError::INVALID_NAME);

    return ParseOk(String(buffer.GetRawCharPtr() - 9, 9));
}

ParseResult<String> ParseMultiNamePath()
{
    MATCH_OP(0x2F);

    uint8_t segCount = buffer[1];
    for(uint8_t i = 0; i < segCount; ++i)
    {
        auto seg = ParseNameSeg();
        if(seg.IsError())
            return ParseError(AMLError::INVALID_NAME);
    }

    uint32_t len = 1 + segCount * 4;
    return ParseOk(String(buffer.GetRawCharPtr() - len, len));
}

ParseResult<String> ParseNullName()
{
    MATCH_OP(0x00);
    return ParseOk(String(buffer.GetRawCharPtr() - 1, 0));
}

ParseResult<String> ParseNamePath()
{
    return ParseTemplate(ParseNullName, ParseNameSeg, ParseDualNamePath, ParseMultiNamePath);
}

ParseResult<String> ParseNameString()
{
    auto before = buffer.GetRawCharPtr();

    if(buffer.ConsumeIf('^'))
        while(buffer.ConsumeIf('^'));
    else
        buffer.ConsumeIf('\\');

    auto namePath = ParseNamePath();
    if(namePath.IsError())
        return ParseError(AMLError::INVALID_NAME);

    return ParseOk(String(before, buffer.GetRawCharPtr() - before));
}

ParseResult<int> ParseDefAlias(Context& ctx)
{
    MATCH_OP(0x06);
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseString(Context& ctx)
{
    MATCH_OP(0x0D);

    uint32_t off = 1;
    while(off < buffer.GetSize() && buffer[off] != '\0')
    {
        if(buffer[off] > 0x7F)
            return ParseError(AMLError::INVALID_STRING);

        ++off;
    }

    // '\0'
    ++off;

    return ParseOk(0);
}

ParseResult<int> ParseConstObj(Context& ctx)
{
    // TODO: actual data of one, ones, zero
    if(buffer[0] == 0x00 || buffer[0] == 0x01 || buffer[0] == 0xFF)
        return ParseOk(0);

    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseRevisionOp(Context& ctx)
{
    if(buffer[0] == 0x5B && buffer[1] == 0x30)
        return ParseOk(0);

    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseComputationalData(Context& ctx)
{
    printf("%u computationaldata\n", buffer[0]);
    switch(buffer[0])
    {
        // Byte const
        case 0x0A:
            return ParseOk(0);
        // Word const
        case 0x0B:
            return ParseOk(0);
        // DWord const
        case 0x0C:
            return ParseOk(0);
        // QWord const
        case 0x0E:
            return ParseOk(0);

        default:
            return ParseTemplate(ctx, ParseString, ParseConstObj, ParseRevisionOp); // XXX TODO:defbuffer
    }
}

ParseResult<int> ParseDefPackage(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseDefVarPackage(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseDataObject(Context& ctx)
{
    return ParseTemplate(ctx, ParseComputationalData, ParseDefPackage, ParseDefVarPackage);
}

ParseResult<int> ParseObjectReference(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseDDBHandle(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseDataRefObject(Context& ctx)
{
    return ParseTemplate(ctx, ParseDataObject, ParseObjectReference, ParseDDBHandle);
}

ParseResult<int> ParseDefName(Context& ctx)
{
    MATCH_OP(0x08);

    auto nameString = ParseNameString();
    FAIL_IF_ERROR(nameString);

    // TODO: test
    auto test = nameString.Get();
    printf("ParseDefName: ");
    test.Print();
    printf("\n");

    auto dataRefObject = ParseDataRefObject(ctx);
    if(dataRefObject.IsError())
        return ParseError(AMLError::INVALID_DEF_NAME);

    // TODO: datarefobj or smth
    assert(false);

    return ParseOk(0);
}

ParseResult<int> ParseDefScope(Context& ctx)
{
    MATCH_OP(0x10);
    /*auto [length, size] = ParsePkgLength();
    printf("Def scope: %u %u\n", length, size);
*/
    assert(false);

    // TODO: (ScopeOp) PkgLength NameString TermList
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseNamespaceModifier(Context& ctx)
{
    return ParseTemplate(ctx, ParseDefAlias, ParseDefName, ParseDefScope);
}

ParseResult<int> ParseNamedObject(Context& ctx)
{
    printf("%x%x\n", buffer[0], buffer[1]);
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseObject(Context& ctx)
{
    return ParseTemplate(ctx, ParseNamespaceModifier, ParseNamedObject);
}

ParseResult<int> ParseType1Op(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseType2Op(Context& ctx)
{
    assert(false);
    return ParseError(AMLError::NO_MATCH);
}

ParseResult<int> ParseTermObj(Context& ctx)
{
    // TODO
    return ParseObject(ctx);
    //return ParseTemplate(ParseObject/*, ParseType1Op, ParseType2Op*/);
}

ParseResult<int> ParseTermList(Context& ctx)
{
    while(buffer.GetSize() > 0)
    {
        auto r = ParseTermObj(ctx);
        if(r.IsError())
        {
            if(r.GetError() == AMLError::NO_MATCH)
                break;
            else
                return ParseError(r.GetError());
        }
    }

    return ParseOk(0);
}

void ParseAML()
{
    // TODO: check checksum and size?
    // TODO: also check sig??

    #if 0
    auto header = reinterpret_cast<const DefBlockHeader*>(buffer.GetRaw());
    #endif

    buffer.Consume(sizeof(DefBlockHeader));

    printf("-----\n");
    Context ctx;

    printf("%p\n", RootNode_.AddChild("\\__SB__")); // TODO: this takes 4 chars.... problem...
    // TODO: FindOrAdd method?



    String s("\\_SB_", 5);
    printf("%p\n", ctx.CurrentNSNode.Find(s));
    printf("-----\n");

    auto res = ParseTermList(ctx);
    assert(res.IsOk()); // TODO: test
    (void)res.Get();
}

int main(int argc, char* argv[])
{
    (void)argc;
    (void)argv;

    std::ifstream file("DSDT", std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    uint8_t* x = new uint8_t[size];
    if(file.read((char*)x, size))
    {
        buffer = Buffer(x, size);
        ParseAML();
    }

    file.close();

    return 0;
}
