#pragma once

#include <cstdint>
#include <type_traits>
#include <utility>

enum class AMLError
{
    NO_MATCH,
    INVALID_NAME,
    INVALID_STRING,
    INVALID_DEF_NAME
};

template<typename E>
struct ParseError
{
    ParseError(const E& error)
        : Error(error) {}

    ParseError(E&& error)
        : Error(std::move(error)) {}

    E Error;
};

template<typename T>
struct ParseOk
{
    ParseOk(const T& data)
        : Data(data) {}

    ParseOk(T&& data)
        : Data(std::move(data)) {}

    T Data;
};

template<typename T, typename E = AMLError>
struct ParseResult
{
    static constexpr auto SIZE  = (sizeof(T) > sizeof(E)) ? sizeof(T) : sizeof(E);
    static constexpr auto ALIGN = (alignof(T) > alignof(E)) ? alignof(T) : alignof(E);

    ParseResult(ParseError<E> e)
    {
        new(&Data_) E(e.Error);
        Ok_ = false;
    }

    ParseResult(ParseOk<T> e)
    {
        new(&Data_) T(e.Data);
        Ok_ = true;
    }

    bool IsOk() const
    {
        return Ok_;
    }

    bool IsError() const
    {
        return !Ok_;
    }

    const T& Get() const
    {
        assert(IsOk());
        return *reinterpret_cast<const T*>(&Data_);
    }

    T& Get()
    {
        assert(IsOk());
        return *reinterpret_cast<T*>(&Data_);
    }

    const E& GetError() const
    {
        assert(IsError());
        return *reinterpret_cast<const E*>(&Data_);
    }

    E& GetError()
    {
        assert(IsError());
        return *reinterpret_cast<E*>(&Data_);
    }

    typename std::aligned_storage<sizeof(T), alignof(T)>::type Data_;
    bool Ok_;
};
