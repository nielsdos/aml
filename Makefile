SOURCES  = $(wildcard *.cpp)
OBJS     = $(patsubst %.cpp, %.o, $(SOURCES))
CPP      = g++

# TODO: Wno-unused-parameter is temporarily
CPPFLAGS = -std=c++17 -Wall -Wextra -Wno-unused-parameter -fno-exceptions -fno-rtti -fstack-protector -s -Os -fsanitize=undefined

.PHONY: all aml run clean

all: aml run

aml: $(OBJS)
	$(CPP) $(CPPFLAGS) $(OBJS) -o aml

run:
	./aml

clean:
	@rm -f $(OBJS)

%.o: %.cpp
	$(CPP) $(CPPFLAGS) -c -o $@ $<
